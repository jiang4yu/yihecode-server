FROM adoptopenjdk/openjdk8:jre8u345-b01
MAINTAINER '465769438@qq.com'
ADD ./camera-ai-zh-server-0.0.1-SNAPSHOT.jar /app-zh.jar
RUN ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo "Asia/Shanghai" > /etc/timezone
EXPOSE 8021
#ENTRYPOINT exec java -Xmx16g -Xms2g -Xmn1g -jar app-zh.jar
ENTRYPOINT exec java -jar app-zh.jar