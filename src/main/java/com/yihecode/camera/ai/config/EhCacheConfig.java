package com.yihecode.camera.ai.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * 描述：EhCache缓存
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
@Configuration
@EnableCaching
public class EhCacheConfig {
}
