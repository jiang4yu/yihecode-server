package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.Location;

/**
 * 摄像头区域节点<树形结构>表
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface LocationMapper extends BaseMapper<Location> {
}
