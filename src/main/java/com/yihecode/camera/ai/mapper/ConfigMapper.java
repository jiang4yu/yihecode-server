package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.Config;

/**
 * 系统配置管理
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface ConfigMapper extends BaseMapper<Config> {
}
