package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.CameraAlgorithm;

/**
 * 摄像头与算法关联管理
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface CameraAlgorithmMapper extends BaseMapper<CameraAlgorithm> {
}