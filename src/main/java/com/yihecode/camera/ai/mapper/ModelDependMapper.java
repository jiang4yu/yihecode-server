package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.ModelDepend;

/**
 * 算法模型依赖管理
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface ModelDependMapper extends BaseMapper<ModelDepend> {

}