package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.WareHouse;

/**
 * 基地管理,海康安防平台摄像头层级目录, 适配中化定制需求
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface WareHouseMapper extends BaseMapper<WareHouse> {
}
