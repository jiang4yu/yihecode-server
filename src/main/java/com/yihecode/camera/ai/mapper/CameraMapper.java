package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.Camera;

/**
 * 摄像头管理
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface CameraMapper extends BaseMapper<Camera> {
}