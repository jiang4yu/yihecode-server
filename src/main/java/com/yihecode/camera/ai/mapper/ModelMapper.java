package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.Model;

/**
 * 算法模型管理
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface ModelMapper extends BaseMapper<Model> {

}