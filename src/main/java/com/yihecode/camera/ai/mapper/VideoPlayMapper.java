package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.VideoPlay;

/**
 * 视频播放控制
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface VideoPlayMapper extends BaseMapper<VideoPlay> {
}
