package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.ReportPeriod;

/**
 * 告警时段控制
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface ReportPeriodMapper extends BaseMapper<ReportPeriod> {
}