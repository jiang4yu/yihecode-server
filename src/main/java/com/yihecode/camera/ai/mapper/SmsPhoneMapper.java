package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.SmsPhone;

/**
 * 短信推送手机号码
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface SmsPhoneMapper extends BaseMapper<SmsPhone> {
}
