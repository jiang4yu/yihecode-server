package com.yihecode.camera.ai.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yihecode.camera.ai.entity.CameraLog;

/**
 * 摄像头取图日志管理， 适配中化定制需求
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public interface CameraLogMapper extends BaseMapper<CameraLog> {
}
