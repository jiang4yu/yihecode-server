package com.yihecode.camera.ai.entity;

import lombok.Data;

@Data
public class Suanfa {
    String fileName;
    String fileSize;
    String md5Str;
    Long fileSizeOriginal;
    String process;
}
