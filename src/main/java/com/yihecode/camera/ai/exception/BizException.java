package com.yihecode.camera.ai.exception;

/**
 * 描述：业务异常类
 *
 * @author zhoumingxing
 * @mail 465769438@qq.com
 */
public class BizException extends Exception {

    public BizException() {
        super();
    }

    public BizException(String message) {
        super(message);
    }
}
